{!! '<' !!}?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach($collection as $item)
        <url>
            <loc>{{$item['url']}}</loc>
            @isset($item['updated_at'])
                <lastmod>{{$item['updated_at']}}</lastmod>
            @endisset
            <changefreq>daily</changefreq>
        </url>
    @endforeach

</urlset>
